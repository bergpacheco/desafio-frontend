import React from "react";
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import Home from './home'
import Cadastro from './cadastro'
import Navbar from './navbar'

export default function BasicExample() {
  return (
      <div>
      <Navbar />
       <BrowserRouter>
        <Switch>
            <Route path="/" exact={true} component={Home} />
            <Route path="/cadastro" exact={true} component={Cadastro} />
        </Switch>
       </BrowserRouter>
      </div>
  );
}