import Swal from "sweetalert2";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { FaTrashAlt } from "@react-icons/all-files/fa/FaTrashAlt";
import { FaRegEdit } from "@react-icons/all-files/fa/FaRegEdit";


export default function Home() {
  const [data, setData] = useState([]);
  useEffect(async () => {
    setData(await getItens());
  }, []);

  const getItens = async () => {
    const { data } = await axios.get("http://localhost:3001/item");
    return data;
  };
  const inserir = async (edit = null) => {
    try {
      const result = await Swal.fire({
        title: edit ? "Editar Item" : "Novo Item",
        html: `<label for="nome">Nome</label><input id="nome" type="text" class="swal2-input" value="${
          edit?.nome || ""
        }" required><br>
          <label for="marca">Marca</label><input id="marca" type="text" class="swal2-input" value="${
            edit?.marca || ""
          }"><br>
          <label for="tipo">Tipo</label><input id="tipo" type="text" class="swal2-input" value="${
            edit?.tipo || ""
          }"><br>
          <label for="qtde">Qtde.</label><input id="qtde" type="number" class="swal2-input" value="${
            edit?.quantidade || 0
          }"><br>
          <label for="preco">Preço</label><input id="preco" type="text" class="swal2-input" value="${
            edit?.preco || ""
          }"><br>`,
        showCancelButton: true,
        confirmButtonText: "Cadastrar",
        confirmButtonColor: "#198754",
        showLoaderOnConfirm: true,
        cancelButtonText: "Cancelar",
        cancelButtonColor: "#DD6B55",
        preConfirm: () => {
          if (
            document.getElementById("nome").value &&
            document.getElementById("marca").value &&
            document.getElementById("tipo").value &&
            document.getElementById("qtde").value &&
            document.getElementById("preco").value
          )
            return {
              nome: document.getElementById("nome").value,
              marca: document.getElementById("marca").value,
              tipo: document.getElementById("tipo").value,
              quantidade: document.getElementById("qtde").value,
              preco: document.getElementById("preco").value,
            };
          else Swal.showValidationMessage("Faltam campos serem preenchidos");
        },
        allowOutsideClick: () => !Swal.isLoading(),
      });

      let form = result.value;

      if (form) if (edit) form["id"] = edit.id;
      axios
        .post("http://localhost:3001/item", form)
        .then(function (response) {
          updateGrid();
        })
        .catch(function (error) {
          console.log(error);
        });
    } catch (e) {
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: "Ocorreu um erro!",
        footer: e,
      });
    }
  };

  const ListarItens = (props) => {
    return (
      <div>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th scope="col">Código</th>
              <th scope="col">Nome</th>
              <th scope="col">Marca</th>
              <th scope="col" style={{ width: "200px", textAlign: "right" }}>Quantidade</th>
              <th scope="col" style={{ width: "200px", textAlign: "right" }}>Preço</th>
              <th scope="col" style={{ width: "120px", textAlign: "center" }}>Ações</th>
            </tr>
          </thead>
          <tbody>
            {props.itens.map((element) => {
              return (
                <tr key={element.id}>
                  <td>{element.id}</td>
                  <td>{element.nome}</td>
                  <td>{element.marca}</td>
                  <td align='right'>{element.quantidade}</td>
                  <td align='right'>{element.preco}</td>
                  
                  <td style={{ width: "120px", textAlign: "center" }}>
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => deleteItem(element.id)}
                    >
                    <FaTrashAlt />
                    </button>
                    <button
                      type="button"
                      className="btn btn-warning ms-1"
                      onClick={() => inserir(element)}
                    >
                     <FaRegEdit />
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  };

  const updateGrid = async () => {
    const data = await getItens();
    setData(data);
  };

  const sendFile = async (event) => {
    let formData = new FormData();
    formData.append("file", event.target.files[0]);
    Swal.showLoading();
    axios
      .post("http://localhost:3001/item/parse", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(function (response) {
        updateGrid();
        Swal.close();
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const deleteItem = async (id) => {
    const res = await axios.delete("http://localhost:3001/item/" + id);
    if (res) updateGrid();
  };
  return (
    <div>
      <div className="container mt-3 text-end">
        <div className="row">
          <div className="col text-start" style={{ paddingTop: "35px" }}>
            <button
              className="btn btn-success"
              type="button"
              onClick={() => inserir()}
            >
              Novo Item
            </button>
          </div>
          <div className="col text-end">
            <div className="mb-3">
              <label htmlFor="formFile" className="form-label">
                Importar Arquivo
              </label>
              <input
                className="form-control"
                type="file"
                id="formFile"
                onChange={sendFile}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container mt-3">
        <ListarItens itens={data} />
      </div>
    </div>
  );
}
